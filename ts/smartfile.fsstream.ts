/*
This file contains logic for streaming things from and to the filesystem
*/
import * as plugins from './smartfile.plugins.js';

export const createReadStream = (pathArg: string) => {
  return plugins.fs.createReadStream(pathArg);
};

export const createWriteStream = (pathArg: string) => {
  return plugins.fs.createWriteStream(pathArg);
};

export const streamDirectory = async (dirPathArg: string) => {};
