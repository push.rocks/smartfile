/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@push.rocks/smartfile',
  version: '10.0.28',
  description: 'offers smart ways to work with files in nodejs'
}
