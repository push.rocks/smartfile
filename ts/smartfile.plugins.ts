// node native scope
import * as fs from 'fs';
import * as path from 'path';

export { fs, path };

// @pushrocks scope
import * as lik from '@push.rocks/lik';
import * as smartfileInterfaces from '@push.rocks/smartfile-interfaces';
import * as smartdelay from '@push.rocks/smartdelay';
import * as smarthash from '@push.rocks/smarthash';
import * as smartjson from '@push.rocks/smartjson';
import * as smartmime from '@push.rocks/smartmime';
import * as smartpath from '@push.rocks/smartpath';
import * as smartpromise from '@push.rocks/smartpromise';
import * as smartrequest from '@push.rocks/smartrequest';
import * as smartstream from '@push.rocks/smartstream';

export {
  lik,
  smartfileInterfaces,
  smartdelay,
  smarthash,
  smartjson,
  smartmime,
  smartpath,
  smartpromise,
  smartrequest,
  smartstream,
};

// third party scope
import fsExtra from 'fs-extra';
import * as glob from 'glob';
import yaml from 'js-yaml';

export { fsExtra, glob, yaml };
