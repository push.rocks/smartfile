import * as smartfile from '../ts/index.js';
import * as path from 'path';

import { expect, tap } from '@pushrocks/tapbundle';

// ---------------------------
// smartfile.fs
// ---------------------------

tap.test('.fs.fileExistsSync -> should return an accurate boolean', async () => {
  // tslint:disable-next-line: no-unused-expression
  expect(smartfile.fs.fileExistsSync('./test/testassets/mytest.json')).toBeTrue();
  // tslint:disable-next-line: no-unused-expression
  expect(smartfile.fs.fileExistsSync('./test/testassets/notthere.json')).toBeFalse();
});

tap.test('.fs.fileExists -> should resolve or reject a promise', async () => {
  expect(smartfile.fs.fileExists('./test/testassets/mytest.json')).toBeInstanceOf(Promise);
  await smartfile.fs.fileExists('./test/testassets/mytest.json');
  await smartfile.fs.fileExists('./test/testassets/notthere.json').catch((err) => {
    return expect(err.message).toEqual(
      "ENOENT: no such file or directory, access './test/testassets/notthere.json'"
    );
  });
});

tap.test('.fs.listFoldersSync() -> should get the file type from a string', async () => {
  expect(smartfile.fs.listFoldersSync('./test/testassets/')).toContain('testfolder');
  expect(smartfile.fs.listFoldersSync('./test/testassets/')).not.toContain('notExistentFolder');
});

tap.test('.fs.listFolders() -> should get the file type from a string', async () => {
  const folderArrayArg = await smartfile.fs.listFolders('./test/testassets/');
  expect(folderArrayArg).toContain('testfolder');
  expect(folderArrayArg).not.toContain('notExistentFolder');
});

tap.test('.fs.listFilesSync() -> should get the file type from a string', async () => {
  expect(smartfile.fs.listFilesSync('./test/testassets/')).toContain('mytest.json');
  expect(smartfile.fs.listFilesSync('./test/testassets/')).not.toContain('notExistentFile');
  expect(smartfile.fs.listFilesSync('./test/testassets/', /mytest\.json/)).toContain('mytest.json');
  expect(smartfile.fs.listFilesSync('./test/testassets/', /mytests.json/)).not.toContain(
    'mytest.json'
  );
});

tap.test('.fs.listFiles() -> should get the file type from a string', async () => {
  const folderArrayArg = await smartfile.fs.listFiles('./test/testassets/');
  expect(folderArrayArg).toContain('mytest.json');
  expect(folderArrayArg).not.toContain('notExistentFile');
});

tap.test('.fs.listFileTree() -> should get a file tree', async () => {
  const folderArrayArg = await smartfile.fs.listFileTree(
    path.resolve('./test/testassets/'),
    '**/*.txt'
  );
  expect(folderArrayArg).toContain('testfolder/testfile1.txt');
  expect(folderArrayArg).not.toContain('mytest.json');
});

tap.test('.fs.fileTreeToObject -> should read a file tree into an Object', async () => {
  const fileArrayArg = await smartfile.fs.fileTreeToObject(
    path.resolve('./test/testassets/'),
    '**/*.txt'
  );
  expect(fileArrayArg[0]).toBeInstanceOf(smartfile.Smartfile);
  expect(fileArrayArg[0].contents.toString()).toEqual(fileArrayArg[0].contentBuffer.toString());
});

tap.test('.fs.copy() -> should copy a directory', async () => {
  await smartfile.fs.copy('./test/testassets/testfolder/', './test/testassets/temp/');
});

tap.test('.fs.copy() -> should copy a file', async () => {
  await smartfile.fs.copy('./test/testassets/mytest.yaml', './test/testassets/temp/mytest.yaml');
});

tap.test('.fs.copy() -> should copy a file and rename it', async () => {
  await smartfile.fs.copy(
    './test/testassets/mytest.yaml',
    './test/testassets/temp/mytestRenamed.yaml'
  );
});

tap.test('.fs.remove() -> should remove an entire directory', async () => {});

tap.test('.fs.remove -> should remove single files', async () => {
  await smartfile.fs.remove('./test/testassets/temp/mytestRenamed.yaml');
});

tap.test('.fs.removeSync -> should remove single files synchronouly', async () => {
  smartfile.fs.removeSync('./test/testassets/temp/testfile1.txt');
  expect(smartfile.fs.fileExistsSync('./test/testassets/temp/testfile1.txt')).toBeFalse();
});

tap.test('.fs.removeMany -> should remove and array of files', async () => {
  smartfile.fs
    .removeMany(['./test/testassets/temp/testfile1.txt', './test/testassets/temp/testfile2.txt'])
    .then(() => {
      expect(smartfile.fs.fileExistsSync('./test/testassets/temp/testfile1.txt')).toBeFalse();
      expect(smartfile.fs.fileExistsSync('./test/testassets/temp/testfile2.txt')).toBeFalse();
    });
});

tap.test('.fs.removeManySync -> should remove and array of single files synchronouly', async () => {
  smartfile.fs.removeManySync([
    './test/testassets/temp/testfile1.txt',
    './test/testassets/temp/testfile2.txt',
  ]);
  expect(smartfile.fs.fileExistsSync('./test/testassets/temp/testfile1.txt')).toBeFalse();
  expect(smartfile.fs.fileExistsSync('./test/testassets/temp/testfile2.txt')).toBeFalse();
});

tap.test('.fs.toObjectSync() -> should read an .yaml file to an object', async () => {
  const testData = smartfile.fs.toObjectSync('./test/testassets/mytest.yaml');
  expect(testData.key1).toEqual('this works');
  expect(testData.key2).toEqual('this works too');
});
tap.test(
  '.fs.toObjectSync() -> should state unknown file type for unknown file types',
  async () => {
    const testData = smartfile.fs.toObjectSync('./test/testassets/mytest.txt');
  }
);

tap.test('.fs.toObjectSync() -> should read an .json file to an object', async () => {
  const testData = smartfile.fs.toObjectSync('./test/testassets/mytest.json');
  expect(testData.key1).toEqual('this works');
  expect(testData.key2).toEqual('this works too');
});

tap.test('.fs.toStringSync() -> should read a file to a string', async () => {
  expect(smartfile.fs.toStringSync('./test/testassets/mytest.txt')).toEqual('Some TestString &&%$');
});

// ---------------------------
// smartfile.interpreter
// ---------------------------

tap.test('.interpreter.filetype() -> should get the file type from a string', async () => {
  expect(smartfile.interpreter.filetype('./somefolder/data.json')).toEqual('json');
});

// ---------------------------
// smartfile.memory
// ---------------------------

tap.test('.memory.toFs() -> should write a file to disk and return a promise', async () => {
  const localString = 'myString';
  await smartfile.memory.toFs(
    localString,
    path.join(process.cwd(), './test/testassets/temp/testMemToFs.txt')
  );
});

tap.test(
  '.memory.toFsSync() -> should write a file to disk and return true if successfull',
  async () => {
    const localString = 'myString';
    smartfile.memory.toFsSync(
      localString,
      path.join(process.cwd(), './test/testassets/temp/testMemToFsSync.txt')
    );
  }
);

// ---------------------------
// smartfile.Smartfile
// ---------------------------

tap.test('.Smartfile -> should produce vinyl compatible files', async () => {
  const smartfileArray = await smartfile.fs.fileTreeToObject(
    process.cwd(),
    './test/testassets/testfolder/**/*'
  );
  const localSmartfile = smartfileArray[0];
  expect(localSmartfile).toBeInstanceOf(smartfile.Smartfile);
  expect(localSmartfile.contents).toBeInstanceOf(Buffer);
  // tslint:disable-next-line:no-unused-expression
  expect(localSmartfile.isBuffer()).toBeTrue();
  // tslint:disable-next-line:no-unused-expression
  expect(localSmartfile.isDirectory()).toBeFalse();
  // tslint:disable-next-line:no-unused-expression
  expect(localSmartfile.isNull()).toBeFalse();
});

tap.test('should output a smartfile array to disk', async () => {
  const smartfileArray = await smartfile.fs.fileTreeToObject('./test/testassets/testfolder/', '*');
  for (const smartfileInstance of smartfileArray) {
    console.log(smartfileInstance.relative);
    console.log(smartfileInstance.path);
    console.log(smartfileInstance.base);
    console.log(smartfileInstance.parsedPath);
  }
  await smartfile.memory.smartfileArrayToFs(
    smartfileArray,
    path.resolve('./test/testassets/temp/testoutput/')
  );
});

tap.test('should create, store and retrieve valid smartfiles', async () => {
  const fileString = 'hi there';
  const filePath = './test/testassets/utf8.txt';
  const smartfileInstance = await smartfile.Smartfile.fromString(filePath, fileString, 'utf8');
  smartfileInstance.write();
  const smartfileInstance2 = await smartfile.Smartfile.fromFilePath(filePath);
  const retrievedString = smartfileInstance.contents.toString();
  expect(retrievedString).toEqual(fileString);
});

tap.test('should get a hash', async () => {
  const fileString = 'hi there';
  const filePath = './test/testassets/utf8.txt';
  const smartfileInstance = await smartfile.Smartfile.fromString(filePath, fileString, 'utf8');
  const hash = await smartfileInstance.getHash();
  console.log(hash);
});

tap.test('should wait for file to be ready', async () => {
  await smartfile.fs.waitForFileToBeReady('./test/testassets/mytest.json');
});

tap.start();
